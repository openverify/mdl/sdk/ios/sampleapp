// swift-interface-format-version: 1.0
// swift-compiler-version: Apple Swift version 5.10 (swiftlang-5.10.0.13 clang-1500.3.9.4)
// swift-module-flags: -target arm64-apple-ios15.0 -enable-objc-interop -enable-library-evolution -swift-version 5 -enforce-exclusivity=checked -O -enable-bare-slash-regex -module-name OpenVerifymDL
import CoreBluetooth
import CryptoKit
import Foundation
import Network
@_exported import OpenVerifymDL
import Security
import Swift
import SwiftUI
import UIKit
import _Concurrency
import _StringProcessing
import _SwiftConcurrencyShims
import os
public enum CBORError : Swift.Error {
  case unfinishedSequence
  case wrongTypeInsideSequence
  case tooLongSequence
  case incorrectUTF8String
  public static func == (a: OpenVerifymDL.CBORError, b: OpenVerifymDL.CBORError) -> Swift.Bool
  public func hash(into hasher: inout Swift.Hasher)
  public var hashValue: Swift.Int {
    get
  }
}
extension OpenVerifymDL.CBOR {
  public static func decode(_ input: [Swift.UInt8], options: OpenVerifymDL.CBOROptions = CBOROptions()) throws -> OpenVerifymDL.CBOR?
}
public class CBORDecoder {
  public var options: OpenVerifymDL.CBOROptions
  public init(stream: any OpenVerifymDL.CBORInputStream, options: OpenVerifymDL.CBOROptions = CBOROptions())
  public init(input: Swift.ArraySlice<Swift.UInt8>, options: OpenVerifymDL.CBOROptions = CBOROptions())
  public init(input: [Swift.UInt8], options: OpenVerifymDL.CBOROptions = CBOROptions())
  public func decodeItem() throws -> OpenVerifymDL.CBOR?
  @objc deinit
}
extension OpenVerifymDL.CBOR {
  public static func encode<T>(_ value: T, options: OpenVerifymDL.CBOROptions = CBOROptions()) -> [Swift.UInt8] where T : OpenVerifymDL.CBOREncodable
  public static func encode<T>(_ array: [T], asByteString: Swift.Bool = false, options: OpenVerifymDL.CBOROptions = CBOROptions()) -> [Swift.UInt8] where T : OpenVerifymDL.CBOREncodable
  public static func encode<A, B>(_ dict: [A : B], options: OpenVerifymDL.CBOROptions = CBOROptions()) -> [Swift.UInt8] where A : OpenVerifymDL.CBOREncodable, A : Swift.Hashable, B : OpenVerifymDL.CBOREncodable
  public static func encodeUInt8(_ x: Swift.UInt8) -> [Swift.UInt8]
  public static func encodeUInt16(_ x: Swift.UInt16) -> [Swift.UInt8]
  public static func encodeUInt32(_ x: Swift.UInt32) -> [Swift.UInt8]
  public static func encodeUInt64(_ x: Swift.UInt64) -> [Swift.UInt8]
  public static func encodeNegativeInt(_ x: Swift.Int64) -> [Swift.UInt8]
  public static func encodeByteString(_ bs: [Swift.UInt8], options: OpenVerifymDL.CBOROptions = CBOROptions()) -> [Swift.UInt8]
  public static func encodeData(_ data: Foundation.Data, options: OpenVerifymDL.CBOROptions = CBOROptions()) -> [Swift.UInt8]
  public static func encodeString(_ str: Swift.String, options: OpenVerifymDL.CBOROptions = CBOROptions()) -> [Swift.UInt8]
  public static func encodeArray<T>(_ arr: [T], options: OpenVerifymDL.CBOROptions = CBOROptions()) -> [Swift.UInt8] where T : OpenVerifymDL.CBOREncodable
  public static func encodeMap<A, B>(_ map: [A : B], options: OpenVerifymDL.CBOROptions = CBOROptions()) -> [Swift.UInt8] where A : OpenVerifymDL.CBOREncodable, A : Swift.Hashable, B : OpenVerifymDL.CBOREncodable
  public static func encodeMap<A>(_ map: [A : Any?], options: OpenVerifymDL.CBOROptions = CBOROptions()) throws -> [Swift.UInt8] where A : OpenVerifymDL.CBOREncodable, A : Swift.Hashable
  public static func encodeTagged<T>(tag: OpenVerifymDL.CBOR.Tag, value: T, options: OpenVerifymDL.CBOROptions = CBOROptions()) -> [Swift.UInt8] where T : OpenVerifymDL.CBOREncodable
  public static func encodeSimpleValue(_ x: Swift.UInt8) -> [Swift.UInt8]
  public static func encodeNull() -> [Swift.UInt8]
  public static func encodeUndefined() -> [Swift.UInt8]
  public static func encodeBreak() -> [Swift.UInt8]
  public static func encodeFloat(_ x: Swift.Float) -> [Swift.UInt8]
  public static func encodeDouble(_ x: Swift.Double) -> [Swift.UInt8]
  public static func encodeBool(_ x: Swift.Bool) -> [Swift.UInt8]
  public static func encodeArrayStreamStart() -> [Swift.UInt8]
  public static func encodeMapStreamStart() -> [Swift.UInt8]
  public static func encodeStringStreamStart() -> [Swift.UInt8]
  public static func encodeByteStringStreamStart() -> [Swift.UInt8]
  public static func encodeStreamEnd() -> [Swift.UInt8]
  public static func encodeArrayChunk<T>(_ chunk: [T], options: OpenVerifymDL.CBOROptions = CBOROptions()) -> [Swift.UInt8] where T : OpenVerifymDL.CBOREncodable
  public static func encodeMapChunk<A, B>(_ map: [A : B], options: OpenVerifymDL.CBOROptions = CBOROptions()) -> [Swift.UInt8] where A : OpenVerifymDL.CBOREncodable, A : Swift.Hashable, B : OpenVerifymDL.CBOREncodable
  public static func encodeDate(_ date: Foundation.Date, options: OpenVerifymDL.CBOROptions = CBOROptions()) -> [Swift.UInt8]
  public static func encodeAny(_ any: Any?, options: OpenVerifymDL.CBOROptions = CBOROptions()) throws -> [Swift.UInt8]
}
public enum CBOREncoderError : Swift.Error {
  case invalidType
  case nonStringKeyInMap
  public static func == (a: OpenVerifymDL.CBOREncoderError, b: OpenVerifymDL.CBOREncoderError) -> Swift.Bool
  public func hash(into hasher: inout Swift.Hasher)
  public var hashValue: Swift.Int {
    get
  }
}
public protocol CBOREncodable {
  func encode(options: OpenVerifymDL.CBOROptions) -> [Swift.UInt8]
  func toCBOR(options: OpenVerifymDL.CBOROptions) -> OpenVerifymDL.CBOR
}
extension OpenVerifymDL.CBOR : OpenVerifymDL.CBOREncodable {
  public func encode(options: OpenVerifymDL.CBOROptions = CBOROptions()) -> [Swift.UInt8]
  public func toCBOR(options: OpenVerifymDL.CBOROptions = CBOROptions()) -> OpenVerifymDL.CBOR
}
extension Swift.Int : OpenVerifymDL.CBOREncodable {
  public func encode(options: OpenVerifymDL.CBOROptions = CBOROptions()) -> [Swift.UInt8]
  public func toCBOR(options: OpenVerifymDL.CBOROptions = CBOROptions()) -> OpenVerifymDL.CBOR
}
extension Swift.Int8 : OpenVerifymDL.CBOREncodable {
  public func encode(options: OpenVerifymDL.CBOROptions = CBOROptions()) -> [Swift.UInt8]
  public func toCBOR(options: OpenVerifymDL.CBOROptions = CBOROptions()) -> OpenVerifymDL.CBOR
}
extension Swift.Int16 : OpenVerifymDL.CBOREncodable {
  public func encode(options: OpenVerifymDL.CBOROptions = CBOROptions()) -> [Swift.UInt8]
  public func toCBOR(options: OpenVerifymDL.CBOROptions = CBOROptions()) -> OpenVerifymDL.CBOR
}
extension Swift.Int32 : OpenVerifymDL.CBOREncodable {
  public func encode(options: OpenVerifymDL.CBOROptions = CBOROptions()) -> [Swift.UInt8]
  public func toCBOR(options: OpenVerifymDL.CBOROptions = CBOROptions()) -> OpenVerifymDL.CBOR
}
extension Swift.Int64 : OpenVerifymDL.CBOREncodable {
  public func encode(options: OpenVerifymDL.CBOROptions = CBOROptions()) -> [Swift.UInt8]
  public func toCBOR(options: OpenVerifymDL.CBOROptions = CBOROptions()) -> OpenVerifymDL.CBOR
}
extension Swift.UInt : OpenVerifymDL.CBOREncodable {
  public func encode(options: OpenVerifymDL.CBOROptions = CBOROptions()) -> [Swift.UInt8]
  public func toCBOR(options: OpenVerifymDL.CBOROptions = CBOROptions()) -> OpenVerifymDL.CBOR
}
extension Swift.UInt8 : OpenVerifymDL.CBOREncodable {
  public func encode(options: OpenVerifymDL.CBOROptions = CBOROptions()) -> [Swift.UInt8]
  public func toCBOR(options: OpenVerifymDL.CBOROptions = CBOROptions()) -> OpenVerifymDL.CBOR
}
extension Swift.UInt16 : OpenVerifymDL.CBOREncodable {
  public func encode(options: OpenVerifymDL.CBOROptions = CBOROptions()) -> [Swift.UInt8]
  public func toCBOR(options: OpenVerifymDL.CBOROptions = CBOROptions()) -> OpenVerifymDL.CBOR
}
extension Swift.UInt32 : OpenVerifymDL.CBOREncodable {
  public func encode(options: OpenVerifymDL.CBOROptions = CBOROptions()) -> [Swift.UInt8]
  public func toCBOR(options: OpenVerifymDL.CBOROptions = CBOROptions()) -> OpenVerifymDL.CBOR
}
extension Swift.UInt64 : OpenVerifymDL.CBOREncodable {
  public func encode(options: OpenVerifymDL.CBOROptions = CBOROptions()) -> [Swift.UInt8]
  public func toCBOR(options: OpenVerifymDL.CBOROptions = CBOROptions()) -> OpenVerifymDL.CBOR
}
extension Swift.String : OpenVerifymDL.CBOREncodable {
  public func encode(options: OpenVerifymDL.CBOROptions = CBOROptions()) -> [Swift.UInt8]
  public func toCBOR(options: OpenVerifymDL.CBOROptions = CBOROptions()) -> OpenVerifymDL.CBOR
}
extension Swift.Float : OpenVerifymDL.CBOREncodable {
  public func encode(options: OpenVerifymDL.CBOROptions = CBOROptions()) -> [Swift.UInt8]
  public func toCBOR(options: OpenVerifymDL.CBOROptions = CBOROptions()) -> OpenVerifymDL.CBOR
}
extension Swift.Double : OpenVerifymDL.CBOREncodable {
  public func encode(options: OpenVerifymDL.CBOROptions = CBOROptions()) -> [Swift.UInt8]
  public func toCBOR(options: OpenVerifymDL.CBOROptions = CBOROptions()) -> OpenVerifymDL.CBOR
}
extension Swift.Bool : OpenVerifymDL.CBOREncodable {
  public func encode(options: OpenVerifymDL.CBOROptions = CBOROptions()) -> [Swift.UInt8]
  public func toCBOR(options: OpenVerifymDL.CBOROptions = CBOROptions()) -> OpenVerifymDL.CBOR
}
extension Swift.Array where Element : OpenVerifymDL.CBOREncodable {
  public func encode(options: OpenVerifymDL.CBOROptions = CBOROptions()) -> [Swift.UInt8]
  public func toCBOR(options: OpenVerifymDL.CBOROptions = CBOROptions()) -> OpenVerifymDL.CBOR
}
extension Swift.Dictionary where Key : OpenVerifymDL.CBOREncodable, Value : OpenVerifymDL.CBOREncodable {
  public func encode(options: OpenVerifymDL.CBOROptions = CBOROptions()) -> [Swift.UInt8]
  public func toCBOR(options: OpenVerifymDL.CBOROptions = CBOROptions()) -> OpenVerifymDL.CBOR
}
extension Swift.Optional where Wrapped : OpenVerifymDL.CBOREncodable {
  public func encode(options: OpenVerifymDL.CBOROptions = CBOROptions()) -> [Swift.UInt8]
  public func toCBOR(options: OpenVerifymDL.CBOROptions = CBOROptions()) -> OpenVerifymDL.CBOR
}
extension Foundation.NSNull : OpenVerifymDL.CBOREncodable {
  public func encode(options: OpenVerifymDL.CBOROptions = CBOROptions()) -> [Swift.UInt8]
  public func toCBOR(options: OpenVerifymDL.CBOROptions = CBOROptions()) -> OpenVerifymDL.CBOR
}
extension Foundation.Date : OpenVerifymDL.CBOREncodable {
  public func encode(options: OpenVerifymDL.CBOROptions = CBOROptions()) -> [Swift.UInt8]
  public func toCBOR(options: OpenVerifymDL.CBOROptions = CBOROptions()) -> OpenVerifymDL.CBOR
}
extension Foundation.Data : OpenVerifymDL.CBOREncodable {
  public func encode(options: OpenVerifymDL.CBOROptions = CBOROptions()) -> [Swift.UInt8]
  public func toCBOR(options: OpenVerifymDL.CBOROptions = CBOROptions()) -> OpenVerifymDL.CBOR
}
public struct ShareLogFile : SwiftUI.UIViewControllerRepresentable {
  public init()
  @objc @_hasMissingDesignatedInitializers @_Concurrency.MainActor(unsafe) public class Coordinator : ObjectiveC.NSObject, UIKit.UINavigationControllerDelegate {
    @objc deinit
  }
  @_Concurrency.MainActor(unsafe) public func makeCoordinator() -> OpenVerifymDL.ShareLogFile.Coordinator
  @_Concurrency.MainActor(unsafe) public func makeUIViewController(context: OpenVerifymDL.ShareLogFile.Context) -> UIKit.UIViewController
  @_Concurrency.MainActor(unsafe) public func updateUIViewController(_ uiViewController: UIKit.UIViewController, context: OpenVerifymDL.ShareLogFile.Context)
  public typealias Body = Swift.Never
  public typealias UIViewControllerType = UIKit.UIViewController
}
@_hasMissingDesignatedInitializers @available(iOS 14.0, *)
public class Verify {
  @available(iOS 14.0, *)
  public func decrypt(encrypted: Foundation.Data, deviceHkdfKey: Swift.String)
  @objc deinit
}
@_hasMissingDesignatedInitializers @available(iOS 14.0, *)
final public class MDL {
  public static let shared: OpenVerifymDL.MDL
  final public func configure(delegate: any OpenVerifymDL.MDLDelegate, qrCode: Swift.String, nameSpaces: [Swift.String : [Swift.String : Swift.Bool]], orgID: Swift.String, licenseKey: Swift.String, debugMode: Swift.Bool, serverRetrieval: Swift.Bool)
  @objc deinit
}
extension OpenVerifymDL.MDL : OpenVerifymDL.MDLDelegate {
  final public func mdlManager(_ mdl: OpenVerifymDL.MDL, verifiedValues: OpenVerifymDL.VerifiedValues)
  final public func mdlManager(_ mdl: OpenVerifymDL.MDL, isReady: Swift.Bool)
  final public func mdlManager(_ mdl: OpenVerifymDL.MDL, isCentralConnected: Swift.Bool)
  final public func mdlManager(_ mdl: OpenVerifymDL.MDL, isRequesting: Swift.Bool)
  final public func mdlManager(_ mdl: OpenVerifymDL.MDL, isResponded: Swift.Bool)
  final public func mdlManager(_ mdl: OpenVerifymDL.MDL, isTerminated: Swift.Bool)
  final public func mdlManager(_ mdl: OpenVerifymDL.MDL, isInternetAvailable: Swift.Bool)
  final public func mdlManager(_ mdl: OpenVerifymDL.MDL, isServerRetrievalAvailable: Swift.Bool)
  final public func mdlManager(_ mdl: OpenVerifymDL.MDL, isServerResponded: Swift.Bool)
  final public func mdlManager(_ mdl: OpenVerifymDL.MDL, verifiedServerValues: OpenVerifymDL.VerifiedServerValues)
}
final public class CodableCBORDecoder {
  final public var useStringKeys: Swift.Bool
  final public var dateStrategy: OpenVerifymDL.DateStrategy
  public init()
  final public var userInfo: [Swift.CodingUserInfoKey : Any]
  final public func decode<T>(_ type: T.Type, from data: Foundation.Data) throws -> T where T : Swift.Decodable
  final public func decode<T>(_ type: T.Type, from data: Swift.ArraySlice<Swift.UInt8>) throws -> T where T : Swift.Decodable
  @objc deinit
}
public protocol CBORInputStream {
  mutating func popByte() throws -> Swift.UInt8
  mutating func popBytes(_ n: Swift.Int) throws -> Swift.ArraySlice<Swift.UInt8>
}
indirect public enum CBOR : Swift.Equatable, Swift.Hashable, Swift.ExpressibleByNilLiteral, Swift.ExpressibleByIntegerLiteral, Swift.ExpressibleByStringLiteral, Swift.ExpressibleByArrayLiteral, Swift.ExpressibleByDictionaryLiteral, Swift.ExpressibleByBooleanLiteral, Swift.ExpressibleByFloatLiteral {
  case unsignedInt(Swift.UInt64)
  case negativeInt(Swift.UInt64)
  case byteString([Swift.UInt8])
  case utf8String(Swift.String)
  case array([OpenVerifymDL.CBOR])
  case map([OpenVerifymDL.CBOR : OpenVerifymDL.CBOR])
  case tagged(OpenVerifymDL.CBOR.Tag, OpenVerifymDL.CBOR)
  case simple(Swift.UInt8)
  case boolean(Swift.Bool)
  case null
  case undefined
  case half(Swift.Float32)
  case float(Swift.Float32)
  case double(Swift.Float64)
  case `break`
  case date(Foundation.Date)
  public func hash(into hasher: inout Swift.Hasher)
  public subscript(position: OpenVerifymDL.CBOR) -> OpenVerifymDL.CBOR? {
    get
    set(x)
  }
  public init(nilLiteral: ())
  public init(integerLiteral value: Swift.Int)
  public init(extendedGraphemeClusterLiteral value: Swift.String)
  public init(unicodeScalarLiteral value: Swift.String)
  public init(stringLiteral value: Swift.String)
  public init(arrayLiteral elements: OpenVerifymDL.CBOR...)
  public init(dictionaryLiteral elements: (OpenVerifymDL.CBOR, OpenVerifymDL.CBOR)...)
  public init(booleanLiteral value: Swift.Bool)
  public init(floatLiteral value: Swift.Float32)
  public static func == (lhs: OpenVerifymDL.CBOR, rhs: OpenVerifymDL.CBOR) -> Swift.Bool
  public struct Tag : Swift.RawRepresentable, Swift.Equatable, Swift.Hashable {
    public let rawValue: Swift.UInt64
    public init(rawValue: Swift.UInt64)
    public var hashValue: Swift.Int {
      get
    }
    public typealias RawValue = Swift.UInt64
  }
  public typealias ArrayLiteralElement = OpenVerifymDL.CBOR
  public typealias BooleanLiteralType = Swift.Bool
  public typealias ExtendedGraphemeClusterLiteralType = Swift.String
  public typealias FloatLiteralType = Swift.Float32
  public typealias IntegerLiteralType = Swift.Int
  public typealias Key = OpenVerifymDL.CBOR
  public typealias StringLiteralType = Swift.String
  public typealias UnicodeScalarLiteralType = Swift.String
  public typealias Value = OpenVerifymDL.CBOR
  public var hashValue: Swift.Int {
    get
  }
}
extension OpenVerifymDL.CBOR.Tag {
  public static let standardDateTimeString: OpenVerifymDL.CBOR.Tag
  public static let epochBasedDateTime: OpenVerifymDL.CBOR.Tag
  public static let positiveBignum: OpenVerifymDL.CBOR.Tag
  public static let negativeBignum: OpenVerifymDL.CBOR.Tag
  public static let decimalFraction: OpenVerifymDL.CBOR.Tag
  public static let bigfloat: OpenVerifymDL.CBOR.Tag
  public static let expectedConversionToBase64URLEncoding: OpenVerifymDL.CBOR.Tag
  public static let expectedConversionToBase64Encoding: OpenVerifymDL.CBOR.Tag
  public static let expectedConversionToBase16Encoding: OpenVerifymDL.CBOR.Tag
  public static let encodedCBORDataItem: OpenVerifymDL.CBOR.Tag
  public static let uri: OpenVerifymDL.CBOR.Tag
  public static let base64Url: OpenVerifymDL.CBOR.Tag
  public static let base64: OpenVerifymDL.CBOR.Tag
  public static let regularExpression: OpenVerifymDL.CBOR.Tag
  public static let mimeMessage: OpenVerifymDL.CBOR.Tag
  public static let uuid: OpenVerifymDL.CBOR.Tag
  public static let selfDescribeCBOR: OpenVerifymDL.CBOR.Tag
}
@_hasMissingDesignatedInitializers final public class Logger {
  public static let shared: OpenVerifymDL.Logger
  @objc deinit
}
public struct VerifiedValues {
  public let verifiedMSOISOMain: Swift.Bool
  public let verifiedDocType: Swift.Bool
  public let verifiedValidity: Swift.Bool
  public let verifyCert: Swift.Bool
  public let coseVerify: Swift.Bool
  public let verifiedMSOISOAamva: Swift.Bool
  public let verifiedMSOISOMainValues: [OpenVerifymDL.CBOR]
  public let verifiedMSOISOAamvaValues: [OpenVerifymDL.CBOR]
}
public struct VerifiedServerValues {
  public let verifiedNotValidBefore: Swift.Bool
  public let verifiedValidity: Swift.Bool
  public let verifiedJSONValues: [Swift.String : Any]
}
@available(iOS 14.0, *)
public protocol MDLDelegate : AnyObject {
  func mdlManager(_ mdl: OpenVerifymDL.MDL, isReady: Swift.Bool)
  func mdlManager(_ mdl: OpenVerifymDL.MDL, isCentralConnected: Swift.Bool)
  func mdlManager(_ mdl: OpenVerifymDL.MDL, isRequesting: Swift.Bool)
  func mdlManager(_ mdl: OpenVerifymDL.MDL, isResponded: Swift.Bool)
  func mdlManager(_ mdl: OpenVerifymDL.MDL, isTerminated: Swift.Bool)
  func mdlManager(_ mdl: OpenVerifymDL.MDL, verifiedValues: OpenVerifymDL.VerifiedValues)
  func mdlManager(_ mdl: OpenVerifymDL.MDL, isInternetAvailable: Swift.Bool)
  func mdlManager(_ mdl: OpenVerifymDL.MDL, isServerRetrievalAvailable: Swift.Bool)
  func mdlManager(_ mdl: OpenVerifymDL.MDL, isServerResponded: Swift.Bool)
  func mdlManager(_ mdl: OpenVerifymDL.MDL, verifiedServerValues: OpenVerifymDL.VerifiedServerValues)
}
public struct CBOROptions {
  public init(useStringKeys: Swift.Bool = false, dateStrategy: OpenVerifymDL.DateStrategy = .taggedAsEpochTimestamp, forbidNonStringMapKeys: Swift.Bool = false)
}
public enum DateStrategy {
  case taggedAsEpochTimestamp
  case annotatedMap
  public static func == (a: OpenVerifymDL.DateStrategy, b: OpenVerifymDL.DateStrategy) -> Swift.Bool
  public func hash(into hasher: inout Swift.Hasher)
  public var hashValue: Swift.Int {
    get
  }
}
public class CodableCBOREncoder {
  public var useStringKeys: Swift.Bool
  public var dateStrategy: OpenVerifymDL.DateStrategy
  public var forbidNonStringMapKeys: Swift.Bool
  public init()
  public func encode(_ value: any Swift.Encodable) throws -> Foundation.Data
  @objc deinit
}
extension OpenVerifymDL.CBORError : Swift.Equatable {}
extension OpenVerifymDL.CBORError : Swift.Hashable {}
extension OpenVerifymDL.CBOREncoderError : Swift.Equatable {}
extension OpenVerifymDL.CBOREncoderError : Swift.Hashable {}
extension OpenVerifymDL.DateStrategy : Swift.Equatable {}
extension OpenVerifymDL.DateStrategy : Swift.Hashable {}
