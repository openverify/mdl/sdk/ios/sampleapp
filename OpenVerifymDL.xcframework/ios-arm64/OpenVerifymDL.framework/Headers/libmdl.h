#include <stdarg.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>

typedef struct ParsedResult {
  char *session_establishment_data;
  char *session_establishment_ereader;
  char *uuid;
  char *device_hkdf_key_hex;
  char *central_json;
  char *peripheral_json;
  char *ble_ident_value;
  char *logs;
} ParsedResult;

typedef struct VerifiedResult {
  char *docType;
  char *Validity;
  char *isomain_mso;
  char *certPath;
  char *certCN;
  char *certST;
  char *cose;
  char *isomain_values;
  char *isoaamva_mso;
  char *isoaamva_values;
} VerifiedResult;

struct ParsedResult *parse_qr_c(const char *qr_code, const char *name_spaces);

struct VerifiedResult *verify_response_c(const char *response, const char *device_hkdf_key_hex);
