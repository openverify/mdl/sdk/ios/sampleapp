//
//  OpenVerifymDL.h
//  OpenVerifymDL
//
//  Created by Madhusudhan Goundla on 4/19/24.
//

#import <Foundation/Foundation.h>
#import <OpenVerifymDL/libmdl.h>
#import <OpenVerifymDL/libmdl_ser.h>

//! Project version number for OpenVerifymDL.
FOUNDATION_EXPORT double OpenVerifymDLVersionNumber;

//! Project version string for OpenVerifymDL.
FOUNDATION_EXPORT const unsigned char OpenVerifymDLVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <OpenVerifymDL/PublicHeader.h>


