#include <stdarg.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>

typedef struct CheckServerAvailable {
  char *server_retrieval_available;
} CheckServerAvailable;

typedef struct ServerRetreival {
  char *url;
  char *request;
} ServerRetreival;

typedef struct VerifiedServerResult {
  char *results;
  char *valid_exp;
  char *valid_nbf;
} VerifiedServerResult;

struct CheckServerAvailable *check_server(const char *qr_code);

struct ServerRetreival *server_retrieval(const char *qr_code, const char *name_spaces);

struct VerifiedServerResult *verify_server(const char *jwt);
