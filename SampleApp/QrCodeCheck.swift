//
//  QrCodeCheck.swift
//  blemdl-copy
//
//  Created by Madhusudhan Goundla on 4/18/24.
//

import Foundation

enum MyError: Error {
    case invalidPrefix(String)
}
func processString(_ input: String) throws {
    // Check if the string starts with the expected prefix
    guard input.hasPrefix("mdoc:") else {
        // If it doesn't start with the expected prefix, throw an error with a custom message
        throw MyError.invalidPrefix("qrcode needs to start with mdoc://")
    }

}
