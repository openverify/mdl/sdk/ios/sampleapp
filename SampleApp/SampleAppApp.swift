//
//  SampleAppApp.swift
//  SampleApp
//
//  Created by Madhusudhan Goundla on 3/21/24.
//

import SwiftUI

@main
struct SampleAppApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
