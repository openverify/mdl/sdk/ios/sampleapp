//
//  CredentialRowPopUpView.swift
//  mdl
//
//  Created by Madhusudhan Goundla on 6/10/24.
//

import SwiftUI

struct CredentialRowPopUpView: View {
    let title: String
    let value: Any // Update the type to Any
    
    var body: some View {
        HStack(spacing: 10) { // Adjust the spacing between key and value
            Text(title)
            Spacer()
            if let stringValue = value as? String {
                Text(stringValue)
                    .lineLimit(1) // Limit the text to one line
                    .truncationMode(.tail) // Add an ellipsis at the end if the text is too long
            } else if let intValue = value as? Int {
                Text(String(intValue)) // Convert Int to String and display
                    .lineLimit(1)
                    .truncationMode(.tail)
            }
            else if let imageValue = value as? UIImage {
                Image(uiImage: imageValue)
                    .resizable()
                    .scaledToFit()
                    .frame(width: 50, height: 50) // Adjust size as needed
            }
        }
        .padding(.vertical, 4)
        .padding(.horizontal, 30) // Add horizontal padding
    }
}
