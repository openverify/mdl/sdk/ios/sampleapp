import SwiftUI
import AVFoundation

struct ScannerView: UIViewControllerRepresentable {
    @Binding var scannedCode: String?
    let scannedCodeManager: ScannedCodeManager
    var didFinishScanning: (() -> Void)? // Callback to stop scanning
    
    // Add didFinishScanning as a parameter in the initializer
    init(scannedCode: Binding<String?>, scannedCodeManager: ScannedCodeManager, didFinishScanning: (() -> Void)?) {
        _scannedCode = scannedCode
        self.scannedCodeManager = scannedCodeManager
        self.didFinishScanning = didFinishScanning
    }
    
    
    func makeUIViewController(context: Context) -> ScannerViewController {
        let viewController = ScannerViewController()
        viewController.delegate = context.coordinator
        return viewController
    }
    
    func updateUIViewController(_ uiViewController: ScannerViewController, context: Context) {
        // Update the view controller if needed
    }
    
    func makeCoordinator() -> Coordinator {
        return Coordinator(scannedCode: $scannedCode, scannedCodeManager: scannedCodeManager, didFinishScanning: didFinishScanning)
    }
    
    class Coordinator: NSObject, ScannerViewControllerDelegate {
        @Binding var scannedCode: String?
        let scannedCodeManager: ScannedCodeManager
        var didFinishScanning: (() -> Void)? // Callback to stop scanning
        
        init(scannedCode: Binding<String?>, scannedCodeManager: ScannedCodeManager, didFinishScanning: (() -> Void)?) {
            _scannedCode = scannedCode
            self.scannedCodeManager = scannedCodeManager
            self.didFinishScanning = didFinishScanning
        }

        func didScanCode(_ code: String) {
            scannedCodeManager.scannedCode = code
            scannedCode = code // Update scannedCode
            didFinishScanning?() // Call the callback to stop scanning
        }
    }
}


protocol ScannerViewControllerDelegate: AnyObject {
    func didScanCode(_ code: String)
}

class ScannerViewController: UIViewController, AVCaptureMetadataOutputObjectsDelegate {
    weak var delegate: ScannerViewControllerDelegate?
    var captureSession: AVCaptureSession!
    var previewLayer: AVCaptureVideoPreviewLayer!
    var isScanning = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .black
        captureSession = AVCaptureSession()
        
        guard let videoCaptureDevice = AVCaptureDevice.default(for: .video) else {
            return
        }
        let videoInput: AVCaptureDeviceInput
        
        do {
            videoInput = try AVCaptureDeviceInput(device: videoCaptureDevice)
        } catch {
            return
        }
        
        if (captureSession.canAddInput(videoInput)) {
            captureSession.addInput(videoInput)
        } else {
            return
        }
        
        let metadataOutput = AVCaptureMetadataOutput()
        
        if (captureSession.canAddOutput(metadataOutput)) {
            captureSession.addOutput(metadataOutput)
            
            metadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
            metadataOutput.metadataObjectTypes = [.qr, .pdf417]
        } else {
            return
        }
        
        previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        previewLayer.frame = view.layer.bounds
        previewLayer.videoGravity = .resizeAspectFill
        view.layer.addSublayer(previewLayer)
        
        captureSession.startRunning()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        if (captureSession.isRunning) {
            captureSession.stopRunning()
        }
    }
    
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
            if !isScanning { // Check if scanning has already occurred
                if let metadataObject = metadataObjects.first {
                    guard let readableObject = metadataObject as? AVMetadataMachineReadableCodeObject else { return }
                    guard let stringValue = readableObject.stringValue else { return }
                    AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
                    print("Scanned code: \(stringValue)")
                    delegate?.didScanCode(stringValue)
                    isScanning = true // Set the flag to indicate scanning has occurred
                }
            }
        }
}
