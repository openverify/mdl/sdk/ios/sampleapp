//
//  ContentView.swift
//  tet
//
//  Created by Madhusudhan Goundla on 5/2/24.
//

import SwiftUI
import AVFoundation
import OpenVerifymDL

struct ContentView: View {
    @ObservedObject private var scannedCodeManager = ScannedCodeManager()
    let buttonWidth: CGFloat = 200
    @State private var isSharePresented: Bool = false
    
    let backgroundColor = Color(red: 0.95, green: 0.955, blue: 0.969)
    
    var body: some View {
        NavigationView {
            ZStack {
                backgroundColor.edgesIgnoringSafeArea(.all)
                
                VStack {
                    // Displaying your logo image
                    Image("oneproof") // Replace with your actual image asset name
                        .resizable()
                        .scaledToFit()
                        .frame(width: 250, height: 300) // Adjust size as needed
                        .padding(.bottom,50)

                    Text("mDL Verify Sample")
                        .foregroundColor(.black)
                        .font(.system(size: 40))
                        .fontWeight(.regular)
                    
                    Spacer()
                    
                    HStack(spacing: 20) {
                        Button(action: {
                            scannedCodeManager.showScanner = true
                        }) {
                            Text("Scan QR Code")
                                .frame(width: buttonWidth)
                                .padding()
                                .foregroundColor(.white)
                                .background(Color.black)
                                .cornerRadius(8)
                                .shadow(color: .gray, radius: 3, x: 0, y: 2)
                        }
                    }
                    .padding(.bottom, 40)
                    
                    #if DEBUG
                    Button("Share Log File") {
                        self.isSharePresented = true
                    }
                    .padding()
                    .sheet(isPresented: $isSharePresented) {
                        ShareLogFile()
                    }
                    #endif
                    
                    Spacer()
                    
                }
            }
            .navigationBarBackButtonHidden(true)
            .sheet(isPresented: $scannedCodeManager.showScanner) {
                ScannerView(scannedCode: $scannedCodeManager.scannedCode, scannedCodeManager: scannedCodeManager) {
                    scannedCodeManager.showScanner = false // Stop scanning
                }
            }
            .background(
                NavigationLink(destination: mDLResultsView(scannedCode: scannedCodeManager.scannedCode ?? ""), isActive: $scannedCodeManager.showResults) {
                    EmptyView()
                }
                    .hidden()
            )
            
            // Place the onChange modifier here within the ZStack
            .onChange(of: scannedCodeManager.scannedCode) { newValue in
                if newValue != nil {
                    scannedCodeManager.showResults = true // Activate navigation
                    print("Scanned QR code Outside: \(newValue!)")
                } else {
                    print("No QR code scanned yet.")
                }
            }
        }
    }
}

class ScannedCodeManager: ObservableObject {
    @Published var scannedCode: String?
    @Published var showScanner = false
    @Published var showResults = false // Control navigation to mDLResultsView
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
