//
//  MDLManager.swift
//  blemdl-copy
//
//  Created by OpenVerify on 18/03/24.
//

import Foundation
import SwiftUI
import OpenVerifymDL

final class MDLManager: ObservableObject {
    
    static var shared = MDLManager()
    static var OpenVerifymDL: MDL = {
        return MDL.shared
    }()
    
    private init() { }
    
    // Device Retireval
    
    @Published var isReady: Bool = false
    @Published var isCentralConnected: Bool = false
    @Published var isRequesting: Bool = false
    @Published var isResponded: Bool = false
    @Published var isTerminated: Bool = false
    
    @Published var isMsoMainVerified: Bool = false
    @Published var isMsoAamvaVerified: Bool = false
    @Published var isDocTypeVerified: Bool = false
    @Published var isValidityVerified: Bool = false
    @Published var isCertVerified: Bool = false
    @Published var isCoseVerified: Bool = false
    @Published var verifiedAge: UInt64 = 0
    @Published var verifiedPhoto: UIImage? = UIImage(named: "defaultImage")
    @Published var verifiedIsoMainValues: [String: Any] = [:]
    @Published var verifiedIsoAamvaValues: [String: Any] = [:]
    
    // Server Retreival
    
    @Published var isInternetAvailable: Bool = false
    @Published var isServerRetrievalAvailable: Bool = false
    @Published var isServerResponded: Bool = false
    @Published var verifiedServerISOMainValues: [String: Any] = [:]
    @Published var verifiedServerISOAAMVAValues: [String: Any] = [:]
    @Published var verifiedNotValidBefore: Bool = false
    @Published var verifiedValidity: Bool = false
    
    // Example if you want to store details in a dictonary for yourself
    
    @Published var dataDict: [String: Any] = [:]
    
    
    let nameSpaces: [String: [String: Bool]] = [
        "org.iso.18013.5.1": [
            "given_name": false,
            "family_name": false,
            "birth_date": false,
            "issuing_country": true,
            "issuing_authority": true,
            "portrait": false
        ],
        "org.iso.18013.5.1.aamva": [
            "domestic_driving_privilege": true,
            "aamva_version": true,
            "race_ethnicity": false,
            "sex": false
        ]
    ]
    
    
    let debug = true
    
    ///// Server Retrieval /////
    
    // If you prefer Server Retrieval over Device Retrieval Turn this into True,
    // Only Handfull of Juridisctions have implemented this
    // This is not Privacy Preserving, only use it if absolutely needed and please let us know if you are planning use it.
    // This is still in Beta, TLS Certificates are needed to be added before you can start using this
    // Hence it is important to talk to use before you use it
    // For Testing, in info.plist add App Transport Security Settings - Allow Arbitrary Loads - Enable
    // The above setting will skip TLS Verification, not recommended for Production.
    // If you are using this, you do not need Device Retireval, you just need internet connection.
    
    let serverRetrieval = false
    
    
    ///// Licensing /////
    
    // App will Crash: Test Keys
    let orgID = "plfS1eb12zCVVAerNLvRLfqmygzcr7g7"
    let licenseKey = "WuSa0hoBiwsrhvVG1A46V9PAcArlbZFH"
    
    ///// Requesting  /////
    
    func requestData(_ str: String) {
        MDLManager.OpenVerifymDL.configure(delegate: self, qrCode: str, nameSpaces: nameSpaces, orgID: orgID, licenseKey: licenseKey, debugMode: debug, serverRetrieval: serverRetrieval)
    }
}

extension MDLManager: MDLDelegate {
    
    // For Device Retrieval
    
    func mdlManager(_ mdl: MDL, isReady: Bool) {
        print("mDOC BLE Ready:\(isReady)")
        self.isReady = isReady
    }
    
    func mdlManager(_ mdl: MDL, isCentralConnected: Bool) {
        print("mDOC Reader is Connected to mDOC Central:\(isCentralConnected)")
        self.isCentralConnected = isCentralConnected
    }
    
    func mdlManager(_ mdl: MDL, isRequesting: Bool) {
        print("mDOC Reader is Requesting:\(isRequesting)")
        self.isRequesting = isRequesting
    }
    
    func mdlManager(_ mdl: MDL, isResponded: Bool) {
        print("mDOC is Responded:\(isResponded)")
        self.isResponded = isResponded
    }
    
    func mdlManager(_ mdl: MDL, isTerminated: Bool) {
        print("mDOC Reader is Termianted BLE:\(isTerminated)")
        self.isTerminated = isTerminated
    }
    
    
    // Check Data Types here https://docs.oneproof.com/verify/mobile-sdks/data-element-types
    
    func mdlManager(_ mdl: OpenVerifymDL.MDL, verifiedValues: OpenVerifymDL.VerifiedValues) {
        print("All Verified Values: \(verifiedValues)")
        self.isMsoMainVerified = verifiedValues.verifiedMSOISOMain
        self.isDocTypeVerified = verifiedValues.verifiedDocType
        self.isValidityVerified = verifiedValues.verifiedValidity
        self.isCertVerified = verifiedValues.verifyCert
        self.isCoseVerified = verifiedValues.coseVerify
        self.isMsoAamvaVerified = verifiedValues.verifiedMSOISOAamva
        let verifiedMSOISOMainValues = verifiedValues.verifiedMSOISOMainValues
        let verifiedMSOISOAamvaValues = verifiedValues.verifiedMSOISOAamvaValues
        
        if verifiedMSOISOMainValues.count > 0 {
            let keysOnly = verifiedMSOISOMainValues.map { $0[0] }
            print("keysOnly:", keysOnly)
            
            let valuesOnly = verifiedMSOISOMainValues.map { $0[1] }
            print("Values only:", valuesOnly)
            
            let dictionary = Dictionary(uniqueKeysWithValues: zip(keysOnly, valuesOnly))
            print("Dictionary:", dictionary)
            
            // Loop through the dictionary
            
            for (key, value) in dictionary {
                // Unwrap key and value
                if case let .utf8String(keyString) = key {
                    switch value {
                    case .utf8String(let stringValue):
                        verifiedIsoMainValues[keyString] = stringValue
                    case .unsignedInt(let intValue):
                        verifiedIsoMainValues[keyString] = intValue
                    case .byteString(let byteValue):
                        verifiedIsoMainValues[keyString] = byteValue
                        // Add more cases if you have other value types
                    default:
                        print("Unsupported value type for key: \(keyString)")
                    }
                }
            }
            
            
            // Example to Access individual values
            //            if let value = dictionary["age_in_years"] as? CBOR {
            //                print("Dictionary value:", value)
            //                if case let .unsignedInt(intString) = value {
            //                    print("unsignedInt value:", intString)
            //                    print("unsignedInt value type:", (type(of: intString)))
            //                    verifiedAge = intString
            //                } else {
            //                    print("Not a unsignedInt")
            //                }
            //            } else {
            //                print("Key not found")
            //            }
            //
            //            if let value = dictionary["portrait"] as? CBOR {
            //                print("Dictionary value:", value)
            //                if case let .byteString(byteString) = value {
            //                    print("ByteString value:", byteString)
            //                    let verifiedPhotoData = Data(byteString)
            //                    verifiedPhoto = UIImage(data: verifiedPhotoData)
            //                } else {
            //                    print("Not a ByteString")
            //                }
            //            } else {
            //                print("Key not found")
            //            }
            
        }
        
        
        if verifiedMSOISOAamvaValues.count > 0 {
            
            let keysOnly = verifiedMSOISOAamvaValues.map { $0[0] }
            print("keysOnly:", keysOnly)
            
            let valuesOnly = verifiedMSOISOAamvaValues.map { $0[1] }
            print("Values only:", valuesOnly)
            
            let dictionary = Dictionary(uniqueKeysWithValues: zip(keysOnly, valuesOnly))
            print("Dictionary:", dictionary)
            
            for (key, value) in dictionary {
                // Unwrap key and value
                if case let .utf8String(keyString) = key {
                    switch value {
                    case .utf8String(let stringValue):
                        verifiedIsoAamvaValues[keyString] = stringValue
                    case .unsignedInt(let intValue):
                        verifiedIsoAamvaValues[keyString] = intValue
                    case .byteString(let byteValue):
                        verifiedIsoAamvaValues[keyString] = byteValue
                        // Add more cases if you have other value types
                    default:
                        print("Unsupported value type for key: \(keyString)")
                    }
                }
            }
        }
    }
    
    // For Server Retrieval
    
    
    func mdlManager(_ mdl: MDL, isInternetAvailable: Bool) {
        print("mDOC Server isInternetAvailable:\(isInternetAvailable)")
        self.isInternetAvailable = isInternetAvailable
    }
    func mdlManager(_ mdl: MDL, isServerRetrievalAvailable: Bool) {
        print("mDOC Server isServerRetrievalAvailable:\(isServerRetrievalAvailable)")
        self.isServerRetrievalAvailable = isServerRetrievalAvailable
    }
    func mdlManager(_ mdl: MDL, isServerResponded: Bool) {
        print("mDOC Server isResponded:\(isServerResponded)")
        self.isServerResponded = isServerResponded
    }
    
    
    // This is for Server Retrieval Only
    // Server Retrieval is based on JSON, so the reply will be JSON based.
    // In the future we will clean this up and consolidate
    // Check Data Types here https://docs.oneproof.com/verify/mobile-sdks/data-element-types
    
    func mdlManager(_ mdl: MDL, verifiedServerValues: VerifiedServerValues) {
        print("mDOC Server verifiedServerValues: \(verifiedServerValues)")
        
        // Parsing the nested verifiedJSONValues
        // "exp" (Expiration Time) Claim
        // https://datatracker.ietf.org/doc/html/rfc7519#section-4.1.4
        
        if let JWTVerifiedValidity = verifiedServerValues.verifiedValidity as? Bool {
            
            print("JWTVerifiedValidity: \(JWTVerifiedValidity)")
            verifiedValidity = JWTVerifiedValidity
        } else {
            print("No data found for 'JWTVerifiedValidity'")
        }
        
        // Parsing the nested verifiedJSONValues
        // https://datatracker.ietf.org/doc/html/rfc7519#section-4.1.5
        // "nbf" (Not Before) Claim
        if let JWTVerifiedNotValidBefore = verifiedServerValues.verifiedNotValidBefore as? Bool {
            
            print("JWTVerifiedNotValidBefore: \(JWTVerifiedNotValidBefore)")
            verifiedNotValidBefore = JWTVerifiedNotValidBefore
            
        } else {
            print("No data found for 'JWTVerifiedValidity'")
        }
        
        
        // Parsing the nested verifiedJSONValues
        // Check Data Types here https://docs.oneproof.com/verify/mobile-sdks/data-element-types
        if let orgValues = verifiedServerValues.verifiedJSONValues["org.iso.18013.5.1"] as? [String: Any] {
            
            // Check for specific keys in the nested dictionary
            // Check the value types
            
            // For example: "age_over_18" is
            if let ageOver18 = orgValues["age_over_18"] as? Bool {
                print("Age Over 18: \(ageOver18)")
                verifiedServerISOMainValues["age_over_18"] = (ageOver18)
            }
            
            if let ageOver21 = orgValues["age_over_21"] as? Bool {
                print("Age Over 21: \(ageOver21)")
                verifiedServerISOMainValues["age_over_21"] = (ageOver21)
                
            }
            
            if let portrait = orgValues["portrait"] as? String {
                print("Portrait: \(portrait)")
                verifiedServerISOMainValues["portrait"] = (portrait)
            } else {
                print("No portrait available.")
            }
        } else {
            print("No data found for 'org.iso.18013.5.1'")
        }
        
        // Example Parsing the nested verifiedJSONValues AAMVA
        
        //        if let orgValues = verifiedServerValues.verifiedJSONValues["org.iso.18013.5.1.aamva"] as? [String: Any] {
        //
        //            // Check for specific keys in the nested dictionary
        //            if let ageOver18 = orgValues["age_over_18"] as? Bool {
        //                print("Age Over 18: \(ageOver18)")
        //                verifiedServerISOAAMVAValues["age_over_18"] = (ageOver18)
        //            }
        //
        //            if let ageOver21 = orgValues["age_over_21"] as? Bool {
        //                print("Age Over 21: \(ageOver21)")
        //                verifiedServerISOAAMVAValues["age_over_21"] = (ageOver21)
        //
        //            }
        //
        //        } else {
        //            print("No data found for 'org.iso.18013.5.1'")
        //        }
    }
    
}
