//
//  PhotoFullScreen.swift
//  age
//
//  Created by Madhusudhan Goundla on 4/30/24.
//

import SwiftUI

struct PhotoFullScreenView: View {
    let image: UIImage
    @Binding var isFullScreen: Bool
    
    var body: some View {
        ZStack {
            Color.black.edgesIgnoringSafeArea(.all)
            
            VStack {
                Image(uiImage: image)
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                    .padding()
                    .onTapGesture {
                        isFullScreen = false
                    }
                
                Button("Close") {
                    isFullScreen = false
                }
                .padding()
                .foregroundColor(.white)
            }
        }
    }
}
