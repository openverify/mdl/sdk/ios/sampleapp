//
//  mDLResultsView.swift
//  age
//
//  Created by Madhusudhan Goundla on 4/25/24.
//
import SwiftUI
struct mDLResultsView: View {
    let scannedCode: String
    
    @StateObject private var mdlManager = MDLManager.shared
    @State private var isReady: Bool = false
    @State private var isCentralConnected: Bool = false
    @State private var isRequesting: Bool = false
    @State private var isResponded: Bool = false
    @State private var isTerminated: Bool = false
    
    @State private var isMsoVerified: Bool = false
    @State private var isDocTypeVerified: Bool = false
    @State private var isValidityVerified: Bool = false
    @State private var isCertVerified: Bool = false
    @State private var isCoseVerified: Bool = false
    
    @State private var showedisCentralConnectedMessage = false
    @State private var showedisRequestingMessage = false
    @State private var showedisTerminatedMessage = false
    @State private var showedisRespondedMessage = false
    //    @State private var showedReadyMessage = false
    
    @State private var showVerifiedResults: Bool = false
    
    // When click will show photo fullscreen
    @State private var isPhotoFullScreen = false
    
    // Results in Key Value
    @State var verifiedIsoMainValues: [String: Any] = [:]
    @State var verifiedIsoAamvaValues: [String: Any] = [:]
    
    // This is for view
//    @State private var isReadyView: Bool = false
    @State private var isCentralConnectedView: Bool = false
    @State private var isRequestingView: Bool = false
    @State private var isRespondedView: Bool = false
    @State private var isTerminatedView: Bool = false
    @State private var isPresentingRequestingSheet = false // This is for popup
    
    let backgroundColor = Color(red: 0.95, green: 0.955, blue: 0.969)
    let customSubTextColor = Color(red: 115 / 255, green: 115 / 255, blue: 115 / 255)
    let customTextColor = Color(red: 42/255, green: 100/255, blue: 184/255)
    let hexColor = Color(red: 3/255, green: 122/255, blue: 210/255)
    
    @Environment(\.presentationMode) var presentationMode
    
    var body: some View {
        NavigationView {
            ZStack(alignment: .top) {
                backgroundColor.edgesIgnoringSafeArea(.all)
                
                if isReady {
                    Text("BLE Services is ON")
                        .font(.headline)
                        .font(.system(size: 24))
                        .multilineTextAlignment(.center)
                        .foregroundColor(customTextColor)
                        .padding(.bottom, 20)
                } else if !isReady {
                    Text("Starting BLE")
                        .font(.headline)
                        .font(.system(size: 24))
                        .multilineTextAlignment(.center)
                        .foregroundColor(customTextColor)
                        .padding(.bottom, 20)
                }
                
                
                
            }
            .onAppear {
                fetchData()
            }
            .onReceive(mdlManager.$isReady) { ready in
                isReady = ready
                print("isReady in front:", isReady)
            }
            .onReceive(mdlManager.$isCentralConnected) { connected in
                isCentralConnected = connected
                print("isCentralConnected in front:", isCentralConnected)
                if isCentralConnected {
                    isPresentingRequestingSheet = true
                }
            }
            .onReceive(mdlManager.$isRequesting) { requesting in
                isRequesting = requesting
                print("isRequesting in front:", isRequesting)
                if isRequesting {
                    isCentralConnected = true
                }
            }
            .onReceive(mdlManager.$isResponded) { responded in
                isResponded = responded
                print("isResponded in front:", isResponded)
                if isResponded {
                    isRequesting = false
                }
                
            }
            .onReceive(mdlManager.$isTerminated) { terminated in
                isTerminated = terminated
                print("isTerminated in front:", isTerminated)
            }
            .onReceive(mdlManager.$verifiedIsoMainValues) { values in
                verifiedIsoMainValues = values
                print("verifiedIsoMainValues in front:", verifiedIsoMainValues)
            }
            .onReceive(mdlManager.$verifiedIsoAamvaValues) { values in
                verifiedIsoAamvaValues = values
                print("verifiedIsoAamvaValues in front:", verifiedIsoAamvaValues)
            }
            .onReceive(mdlManager.$isMsoMainVerified) { values in
                isMsoVerified = values
            }
            .onReceive(mdlManager.$isDocTypeVerified) { values in
                isDocTypeVerified = values
            }
            .onReceive(mdlManager.$isValidityVerified) { values in
                isValidityVerified = values
            }
            .onReceive(mdlManager.$isCertVerified) { values in
                isCertVerified = values
            }
            .onReceive(mdlManager.$isCoseVerified) { values in
                isCoseVerified = values
            }
            .sheet(isPresented: $isPresentingRequestingSheet, onDismiss: {
            }) {
                ZStack{
                    backgroundColor.edgesIgnoringSafeArea(.all)
                    
                    VStack(alignment: .leading){
                        
                        VStack {
                            
                            if isCentralConnected {
                                VStack {
                                    Text("Successfully Connected to Reader")
                                        .font(.headline)
                                        .font(.system(size: 24))
                                        .multilineTextAlignment(.center)
                                        .foregroundColor(customTextColor)
                                        .padding(.top, 20)
                                }
                            }
                            else if isRequesting {
                                VStack {
                                    Text("Reader is Requesting Information")
                                        .font(.headline)
                                        .font(.system(size: 24))
                                        .multilineTextAlignment(.center)
                                        .foregroundColor(customTextColor)
                                        .padding(.top, 20)
                                }
                            }
                            
                            else if isResponded {
                                VStack {
                                    Text("Your Wallet is Responding to Reader")
                                        .font(.headline)
                                        .font(.system(size: 24))
                                        .multilineTextAlignment(.center)
                                        .foregroundColor(customTextColor)
                                        .padding(.top, 20)
                                }
                            }
                            
                            else if isTerminated {
                                Text("All connections have been terminated")
                                    .font(.callout)
                                    .padding()
                                    .foregroundColor(.red)
                            }
                            
                        }
                        
                        ScrollView {
                            VStack(alignment: .leading, spacing: 20) {
                                if !verifiedIsoMainValues.isEmpty {
                                    VStack(alignment: .center) {
                                        Text("org.iso.18013.5.1")
                                            .font(.headline)
                                            .foregroundColor(.white)
                                            .frame(maxWidth: .infinity)
                                            .padding()
                                            .background(Color.blue.opacity(0.3))
                                            .padding(.bottom)
                                    }
                                    ForEach(verifiedIsoMainValues.sorted(by: { $0.key < $1.key }), id: \.key) { key, value in
                                        if key == "portrait", let data = value as? [UInt8], let image = fetchProfileImageOnly(data: data) {
                                            CredentialRowPopUpView(title: key, value: image)
                                        } else {
                                            CredentialRowPopUpView(title: key, value: value)
                                        }
                                    }
                                }
                                
                                if !verifiedIsoAamvaValues.isEmpty {
                                    Text("org.iso.18013.5.1.aamva")
                                        .font(.headline)
                                        .foregroundColor(.white)
                                        .frame(maxWidth: .infinity)
                                        .padding()
                                        .background(Color.blue.opacity(0.3))
                                        .padding(.bottom)
                                    
                                    ForEach(verifiedIsoAamvaValues.sorted(by: { $0.key < $1.key }), id: \.key) { key, value in
                                        CredentialRowPopUpView(title: key, value: value)
                                    }
                                    
                                }
                            }
                            .padding()
                            .frame()
                            
                            if !verifiedIsoAamvaValues.isEmpty {
                                VStack{
                                    HStack {
                                        Text("MSO Verified")
                                            .font(.headline)
                                            .padding()
                                            .foregroundColor(Color.black)
                                        Spacer()
                                        if isMsoVerified {
                                            Image(systemName: "checkmark.circle.fill")
                                                .foregroundColor(.green)
                                                .frame(width: 20, height: 20)
                                                .padding(.trailing)
                                        } else {
                                            Image(systemName: "xmark.circle.fill")
                                                .foregroundColor(.red)
                                                .frame(width: 20, height: 20)
                                                .padding(.trailing)
                                        }
                                    }
                                    
                                    HStack {
                                        Text("DocType Verified")
                                            .font(.headline)
                                            .padding()
                                            .foregroundColor(Color.black)
                                        Spacer()
                                        if isDocTypeVerified {
                                            Image(systemName: "checkmark.circle.fill")
                                                .foregroundColor(.green)
                                                .frame(width: 20, height: 20)
                                                .padding(.trailing)
                                        } else {
                                            Image(systemName: "xmark.circle.fill")
                                                .foregroundColor(.red)
                                                .frame(width: 20, height: 20)
                                                .padding(.trailing)
                                        }
                                    }
                                    
                                    HStack {
                                        Text("Validity Verified")
                                            .font(.headline)
                                            .padding()
                                            .foregroundColor(Color.black)
                                        Spacer()
                                        if isValidityVerified {
                                            Image(systemName: "checkmark.circle.fill")
                                                .foregroundColor(.green)
                                                .frame(width: 20, height: 20)
                                                .padding(.trailing)
                                        } else {
                                            Image(systemName: "xmark.circle.fill")
                                                .foregroundColor(.red)
                                                .frame(width: 20, height: 20)
                                                .padding(.trailing)
                                        }
                                    }
                                    
                                    HStack {
                                        Text("Certificate Verified")
                                            .font(.headline)
                                            .padding()
                                            .foregroundColor(Color.black)
                                        Spacer()
                                        if isCertVerified {
                                            Image(systemName: "checkmark.circle.fill")
                                                .foregroundColor(.green)
                                                .frame(width: 20, height: 20)
                                                .padding(.trailing)
                                        } else {
                                            Image(systemName: "xmark.circle.fill")
                                                .foregroundColor(.red)
                                                .frame(width: 20, height: 20)
                                                .padding(.trailing)
                                        }
                                    }
                                    
                                    HStack {
                                        Text("Cose Verified")
                                            .font(.headline)
                                            .padding()
                                            .foregroundColor(Color.black)
                                        Spacer()
                                        if isCoseVerified {
                                            Image(systemName: "checkmark.circle.fill")
                                                .foregroundColor(.green)
                                                .frame(width: 20, height: 20)
                                                .padding(.trailing)
                                        } else {
                                            Image(systemName: "xmark.circle.fill")
                                                .foregroundColor(.red)
                                                .frame(width: 20, height: 20)
                                                .padding(.trailing)
                                        }
                                    }
                                    
                                }
                            }
                        }
                        
                        Spacer()
                        if isTerminated {
                            Button(action: {
                                presentationMode.wrappedValue.dismiss()
                                isPresentingRequestingSheet = false
                            }) {
                                Text("Done")
                                    .foregroundColor(.blue)
                            }
                            .padding()
                            .background(Color.clear)
                            .frame(maxWidth: .infinity, alignment: .bottom)
                        }
                    }
                }
                
            }
            .padding()
            .background(Color(red: 0.99, green: 0.99, blue: 0.99))
            .cornerRadius(8)
            .shadow(radius: 2)
            .frame(width: 380, height: 550)
            .frame(maxHeight: .infinity)
            
        }
    }
    
    func fetchData() {
        // Call your method to fetch data here
        MDLManager.shared.requestData(scannedCode)
    }
    
    private func fetchProfileImageOnly(data: [UInt8]) -> UIImage? {
        let portraitData = Data(data)
        let profileImage = UIImage(data: portraitData)
        return profileImage
    }
    
}

//
//VStack(alignment: .leading) {
//
//
//    //                    Spacer()
//    VStack{
////
////                        ForEach(verifiedIsoMainValues.sorted(by: { $0.key < $1.key }), id: \.key) { key, value in
////                            if key == "portrait", let data = value as? [UInt8], let image = fetchProfileImageOnly(data: data) {
////                                CredentialRowPopUpView(title: key, value: image)
////                            } else {
////                                CredentialRowPopUpView(title: key, value: value)
////                            }
////                        }
//
//
//
//
//        //                        HStack {
//        //                            Text("MSO Verified")
//        //                                .font(.headline)
//        //                                .padding()
//        //                                .foregroundColor(Color.black)
//        //                            Spacer()
//        //                            Circle()
//        //                                .foregroundColor(isMsoVerified ? .green : .red) // Conditionally set color
//        //                                .frame(width: 20, height: 20)
//        //                                .padding(.trailing)
//        //                        }
//
//        //                        HStack {
//        //                            Text("DocType Verified")
//        //                                .font(.headline)
//        //                                .padding()
//        //                                .foregroundColor(Color.black)
//        //                            Spacer()
//        //                            Circle()
//        //                                .foregroundColor(isDocTypeVerified ? .green : .red) // Conditionally set color
//        //                                .frame(width: 20, height: 20)
//        //                                .padding(.trailing)
//        //                        }
//
//        //                        HStack {
//        //                            Text("Validity Verified")
//        //                                .font(.headline)
//        //                                .padding()
//        //                                .foregroundColor(Color.black)
//        //                            Spacer()
//        //                            Circle()
//        //                                .foregroundColor(isValidityVerified ? .green : .red) // Conditionally set color
//        //                                .frame(width: 20, height: 20)
//        //                                .padding(.trailing)
//        //                        }
//
//        //                        HStack {
//        //                            Text("Certificate Verified")
//        //                                .font(.headline)
//        //                                .padding()
//        //                                .foregroundColor(Color.black)
//        //                            Spacer()
//        //                            Circle()
//        //                                .foregroundColor(isCertVerified ? .green : .red) // Conditionally set color
//        //                                .frame(width: 20, height: 20)
//        //                                .padding(.trailing)
//        //                        }
//
//        //                        HStack {
//        //                            Text("Cose Verified")
//        //                                .font(.headline)
//        //                                .padding()
//        //                                .foregroundColor(Color.black)
//        //                            Spacer()
//        //                            Circle()
//        //                                .foregroundColor(isCoseVerified ? .green : .red) // Conditionally set color
//        //                                .frame(width: 20, height: 20)
//        //                                .padding(.trailing)
//        //                        }
//
//
//
//    }
//    .onReceive(mdlManager.$isMsoMainVerified) { values in
//        isMsoVerified = values
//    }
//    .onReceive(mdlManager.$isDocTypeVerified) { values in
//        isDocTypeVerified = values
//    }
//    .onReceive(mdlManager.$isValidityVerified) { values in
//        isValidityVerified = values
//    }
//    .onReceive(mdlManager.$isCertVerified) { values in
//        isCertVerified = values
//    }
//    .onReceive(mdlManager.$isCoseVerified) { values in
//        isCoseVerified = values
//    }
//    .padding(.horizontal)
//    .background(Color(red: 0.95, green: 0.955, blue: 0.969))
//    .cornerRadius(8)
//    .shadow(radius: 2)
//    .frame(maxWidth: .infinity)
//    Spacer()
//
//}
//.navigationBarBackButtonHidden(true)
//.padding()
//.padding(.top, 20)
