# SampleApp

## Getting started

Add Framework to your app.

Copy Items as needed helps or else xcode might not find the framework.

## Follow the below documentation

https://docs.openverify.app/release-v.0.0.3/verify-mdl/embedding-the-framework

## Pass QRCode from your Camera

**stringValue** being the qrcode that was scanned from a mDL which starts with mdoc: or mdoc://

```
MDLManager.shared.requestData(stringValue)
```
